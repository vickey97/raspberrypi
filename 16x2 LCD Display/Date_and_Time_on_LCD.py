#Author: Vivek Patel
#Last Edited: 29/10/2018
#This program prints Date and Time on 16x2 LCD on mentioned position, Also works on other LCDs

from RPLCD import CharLCD, cleared, cursor #importing RPLCD library to use our LCD
import RPi.GPIO as GPIO #importing GPIO library of RaspberryPi
import time #importing time library to add delay

GPIO.setmode(GPIO.BOARD) #Setting GPIO Pin Numbering as BOARD
GPIO.setup(29, GPIO.OUT) #setting pin 29 of GPIO as output
GPIO.setup(15, GPIO.OUT) #setting pin 15 of GPIO as output
GPIO.setup(11, GPIO.OUT) #setting pin 11 of GPIO as output
GPIO.setup(13, GPIO.OUT) #setting pin 13 of GPIO as output
GPIO.setup(32, GPIO.OUT) #setting pin 32 of GPIO as output
GPIO.setup(22, GPIO.OUT) #setting pin 22 of GPIO as output
GPIO.setup(18, GPIO.OUT) #setting pin 18 of GPIO as output
GPIO.setup(16, GPIO.OUT) #setting pin 16 of GPIO as output

lcd = CharLCD(numbering_mode=GPIO.BOARD, cols=16, rows=2, pin_rs=33, pin_e=31, pins_data=[29,15,11,13,32,22,18,16]) #Setting LCD parameters and it's data pins

def main():
    #lcd.cursor_pos = (0, 0)
    #lcd.write_string("GPIO Ver: %s" %GPIO.VERSION)
    lcd.cursor_pos = (0, 0) #Setting LCD Cursor Position
    lcd.write_string("Date: %s" %time.strftime("%d/%m/%Y")) #Writing date on LCD
    lcd.cursor_pos = (1, 0) #Setting LCD Cursor Position
    lcd.write_string("Time: %s" %time.strftime("%H:%M:%S")) #Writing time on LCD
try:
    lcd.clear() #Cleaning LCD
    while True:
        main() #calling main() function
        
except:
    pass

finally:
    lcd.clear() #Clearing LCD at the end
    GPIO.cleanup() #Cleaning Up GPIO pins for Safety
