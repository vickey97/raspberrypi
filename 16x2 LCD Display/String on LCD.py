#Author: Vivek Patel
#Last Edited: 29/07/2018
#This program prints string on 16x2 LCD on mentioned position, Also works on other LCDs 

import time #importing time library to add delay
from RPLCD import CharLCD #importing RPLCD library to use our LCD
import RPi.GPIO as GPIO #importing GPIO library of RaspberryPi

GPIO.setmode(GPIO.BCM) #Setting GPIO Pin Numbering as BCM
GPIO.setup(5, GPIO.OUT) #setting pin 5 of GPIO as output
GPIO.setup(22, GPIO.OUT) #setting pin 22 of GPIO as output
GPIO.setup(17, GPIO.OUT) #setting pin 17 of GPIO as output
GPIO.setup(27, GPIO.OUT) #setting pin 27 of GPIO as output
GPIO.setup(12, GPIO.OUT) #setting pin 12 of GPIO as output
GPIO.setup(25, GPIO.OUT) #setting pin 25 of GPIO as output
GPIO.setup(24, GPIO.OUT) #setting pin 24 of GPIO as output
GPIO.setup(23, GPIO.OUT) #setting pin 23 of GPIO as output

lcd = CharLCD(numbering_mode=GPIO.BCM, cols=16, rows=2, pin_rs=13, pin_e=6, pins_data=[5,22,17,27,12,25,24,23]) #Setting LCD parameters and it's data pins

try:
    lcd.clear() #Cleaning LCD
    lcd.cursor_pos = (0, 0) #Setting LCD Cursor Position
    lcd.write_string(u'Hey There!') #Writing string on LCD
    time.sleep(1) #Waiting for 1 sec
    lcd.cursor_pos = (1, 0)  #Setting LCD Cursor Position
    lcd.write_string(u'It is Working') #Writing string on LCD

except:
    pass

finally:
    lcd.clear() #Clearing LCD at the end
    GPIO.cleanup() #Cleaning Up GPIO pins for Safety