#Author: Vivek Patel
#Last Edited: 29/10/2018
#This program creates an interactive menu on 16x2 LCD, Also works on other LCDs
#Menu works this way: There are two options on main LCD like Footware, Transport. When Upper button is pressed, inner menu of Footware displays.
#There are three buttons to navigate into the Menu.
#Back Button: To get out of the inner menu.
#Upper Button: To get inside menu of upper item.
#Lower Button: To get inside menu of lower item.

from RPLCD.gpio import CharLCD #importing RPLCD library to use our LCD
import RPi.GPIO as GPIO #importing GPIO library of RaspberryPi
import time #importing time library to add delay

GPIO.setmode(GPIO.BCM) #Setting GPIO Pin Numbering as BOARD
GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #setting pin 26 of GPIO as input for button, Used to Select Upper Menu
GPIO.setup(19, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #setting pin 19 of GPIO as input for button, Used to Select Lower Menu
GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #setting pin 20 of GPIO as input for button, Used to Go Back

lcd = CharLCD(numbering_mode=GPIO.BCM, cols=16, rows=2, pin_rw=None, pin_rs=13, pin_e=6, pins_data=[5,22,17,27,12,25,24,23], 
charmap='A02', auto_linebreaks=False) #Setting LCD parameters and it's data pins

#Global Variables
update = 1 # Causes LCD to be updated when set to 1
mlevel = 1 # Current menu level
blevel = 1 # Last menu level
  
def level1():
    #main menu
    lcd.cursor_pos = (0, 0)
    lcd.write_string("1.Footware")
    lcd.cursor_pos = (1, 0)
    lcd.write_string("2.Transport")
    
def level2():
    #sub menu 
    lcd.cursor_pos = (0, 0)
    lcd.write_string("1.Shoes")
    lcd.cursor_pos = (1, 0)
    lcd.write_string("2.Boots")
    
def level3():
    #sub menu 
    lcd.cursor_pos = (0, 0)
    lcd.write_string("1.Ground")
    lcd.cursor_pos = (1, 0)
    lcd.write_string("2.Air")
    
def level4():
    #sub menu
    lcd.cursor_pos = (0, 0)
    lcd.write_string("1.Red")
    lcd.cursor_pos = (1, 0)
    lcd.write_string("2.Black")
    
def level5():
    #sub menu 
    lcd.cursor_pos = (0, 0)
    lcd.write_string("1.Lace up")
    lcd.cursor_pos = (1, 0)
    lcd.write_string("2.Zip up")
    
def level6():
    #sub menu 
    lcd.cursor_pos = (0, 0)
    lcd.write_string("1.Car")
    lcd.cursor_pos = (1, 0)
    lcd.write_string("2.Bus") 
    
def level7():
    #sub menu 
    lcd.cursor_pos = (0, 0)
    lcd.write_string("1.Plane")
    lcd.cursor_pos = (1, 0)
    lcd.write_string("2.Helicopter")   
    
def option1(channel):
    global mlevel, update, blevel
    blevel = mlevel
    mlevel = mlevel*2
    if mlevel > 6: mlevel = blevel #error checking
    update = 1
    
    
def option2(channel):
    global mlevel, update, blevel
    blevel = mlevel  
    mlevel = (mlevel*2) + 1
    if mlevel > 7: mlevel = blevel #error checking
    update = 1  
    
def goback(channel):
    global mlevel, update, blevel
    mlevel = blevel
    blevel = int(mlevel/2)
    if blevel < 1: blevel = 1 #error checking
    update = 1
    
GPIO.add_event_detect(26, GPIO.RISING, callback=option1, bouncetime=200) #Detects rising event on pin 26 and calls its function, bouncetime is 200ms
GPIO.add_event_detect(19, GPIO.RISING, callback=option2, bouncetime=200) #Detects rising event on pin 19 and calls its function, bouncetime is 200ms
GPIO.add_event_detect(20, GPIO.RISING, callback=goback, bouncetime=200) #Detects rising event on pin 20 and calls its function, bouncetime is 200ms

try:
    #Main Loop to update menu on LCD
    while True:
        while update ==0:
            time.sleep (0.1)
            
         
        lcd.clear() #Clearing LCD
        if mlevel == 1: level1()
        if mlevel == 2: level2()
        if mlevel == 3: level3()
        if mlevel == 4: level4()
        if mlevel == 5: level5()
        if mlevel == 6: level6()
        if mlevel == 7: level7()
                
        update = 0
except:
    pass

finally:
    lcd.clear() #Clearing LCD at the end
    GPIO.cleanup() #Cleaning Up GPIO pins for Safety