#Author: Vivek Patel
#Last Edited: 1/01/2018

import pygame #importing pygame library to detect button events

pygame.init() #initializing pygame

j = pygame.joystick.Joystick(0)
j.init()

try:
    while True:
        events = pygame.event.get() #detects button events
        for event in events:
            if event.type == pygame.JOYBUTTONDOWN: #when Button is pressed
                #print("Button Pressed")
                if j.get_button(6)==1: #L2 Button
                    print("L2")
                elif j.get_button(7)==1: #R2 Button
                    print("R2")
                elif j.get_button(0)==1: #X Button
                    print("X")
                elif j.get_button(1)==1: #Circle Button
                    print("Circle")
                elif j.get_button(2)==1: #Triangle Button
                    print("Triangle")
                elif j.get_button(3)==1: #Square Button
                    print("Square")
                elif j.get_button(4)==1: #L1 Button
                    print("L1")
                elif j.get_button(5)==1: #R1 Button
                    print("R1")
                elif j.get_button(8)==1: #Share Button
                    print("Share")
                elif j.get_button(9)==1: #Options Button
                    print("Options")
                elif j.get_button(10)==1: #PS Button
                    print("PS")
                elif j.get_button(11)==1: #Left Joystick Button
                    print("Left AP")
                elif j.get_button(12)==1: #Right Joystick Button
                    print("Right AP")
                elif j.get_button(13)==1: #Touchpad Button
                    print("Touchpad")
            elif event.type == pygame.JOYBUTTONUP: #when button is released
                print("Nothing Pressed")

except KeyboardInterrupt:
    print("EXITING NOW")
    j.quit() #Quitting pygame
