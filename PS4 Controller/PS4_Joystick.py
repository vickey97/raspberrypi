#Author: Vivek Patel
#Last Edited: 4/01/2018

import pygame #importing pygame library to detect button events
import time #importing time library for delay

pygame.init() #initializing pygame
pygame.joystick.init() #initializing joystick

joystick = pygame.joystick.Joystick(0)
joystick.init()

def joy(): #user defined function for joystick
    while 1:
        time.sleep(0.05) #delay of 0.05ms for accuracy
        pygame.event.get() #detects events
        LR1 = joystick.get_axis(0) #gets Left Right co-ordiantes of left joystick
        UD1 = joystick.get_axis(1) #gets Up Down co-ordiantes of left joystick
        LR2 = joystick.get_axis(3) #gets Left Right co-ordiantes of right joystick
        UD2 = joystick.get_axis(4) #gets Up Down co-ordiantes of right joystick
        print("Left Joystick: ({:>6.3f},{:>6.3f})".format(LR1,UD1), end="        ")
        print("Right Joystick:({:>6.3f},{:>6.3f})".format(LR2,UD2)) #prints obtained co=ordinates of joystick
        
try:
    while 1: #infinite loop
        joy() #calls joy() function
except:
    pass
finally:
    jostick.quit()
