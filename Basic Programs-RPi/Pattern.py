#Author: Vivek Patel
#Last Edited: 29/07/2018

import time #importing time library to add delay
import RPi.GPIO as GPIO #importing GPIO library of RaspberryPi
GPIO.setmode(GPIO.BOARD) #Setting GPIO Pin Numbering as BOARD
GPIO.setup(7,GPIO.OUT,initial=GPIO.LOW) #setting pin 7 of GPIO as output, Initially off
GPIO.setup(11,GPIO.OUT,initial=GPIO.LOW) #setting pin 11 of GPIO as output, Initially off
GPIO.setup(15,GPIO.OUT,initial=GPIO.LOW) #setting pin 15 of GPIO as output, Initially off
GPIO.setup(37,GPIO.OUT,initial=GPIO.LOW) #setting pin 37 of GPIO as output, Initially off

delay=0.5
first=[7,37,11,15] #List of sequence
second=[15,11,37,7] #List of reverse sequence

try:
    while 1:
        for i in first:
            GPIO.output(i,GPIO.HIGH) #Turning LED on
            time.sleep(delay) #Waiting for 0.5 sec
            GPIO.output(i,GPIO.LOW) #Turning LED off
            time.sleep(delay) #Waiting for 0.5 sec
        for j in second:
            GPIO.output(j,GPIO.HIGH) #Turning LED on
            time.sleep(delay) #Waiting for 0.5 sec
            GPIO.output(j,GPIO.LOW) #Turning LED off
            time.sleep(delay) #Waiting for 0.5 sec
            
except KeyboardInterrupt:
    pass

finally:
    GPIO.cleanup() #Cleaning Up GPIO pins for Safety
