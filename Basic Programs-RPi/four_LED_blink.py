#Author: Vivek Patel
#Last Edited: 29/07/2018

import time #importing time library to add delay
import RPi.GPIO as GPIO #importing GPIO library of RaspberryPi

GPIO.setmode(GPIO.BOARD) #Setting GPIO Pin Numbering as BOARD
GPIO.setup(7, GPIO.OUT) #setting pin 7 of GPIO as output
GPIO.setup(11, GPIO.OUT) #setting pin 11 of GPIO as output
GPIO.setup(13, GPIO.OUT) #setting pin 13 of GPIO as output
GPIO.setup(15, GPIO.OUT) #setting pin 15 of GPIO as output

pin=7,11,13,15 #Setting pin numbers in pin variable
delay=0.5 #Setting 0.5 sec delay

try:
    while True:
        GPIO.output(pin, GPIO.LOW) #Turning LED on
        time.sleep(delay) #Waiting for 0.5 sec
        GPIO.output(pin, GPIO.HIGH) #Turning LED off
        time.sleep(delay) #Waiting for 0.5 sec

except KeyboardInterrupt:
    pass

finally:
    GPIO.cleanup() #Cleaning Up GPIO pins for Safety