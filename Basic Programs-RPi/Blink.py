#Author: Vivek Patel
#Last Edited: 29/07/2018

import RPi.GPIO as GPIO #importing GPIO library of RaspberryPi
import time #importing time library to add delay

GPIO.setmode(GPIO.BOARD) #Setting GPIO Pin Numbering as BOARD
GPIO.setup(7,GPIO.OUT) #setting pin 7 of GPIO as output

try:
    while 1: #Infinite Loop
        GPIO.output(7,GPIO.HIGH) #Turning LED on
        time.sleep(1) #Waiting for 1 sec
        GPIO.output(7,GPIO.LOW) #Turning LED off
        time.sleep(1) #Waiting for 1 sec

except:
    pass

finally:
    GPIO.cleanup() #Cleaning Up GPIO pins for Safety