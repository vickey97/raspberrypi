#Author: Vivek Patel
#Last Edited: 31/08/2018

import RPi.GPIO as GPIO #importing GPIO library of RaspberryPi
import time #importing time library to add delay

GPIO.setmode(GPIO.BOARD) #Setting GPIO Pin Numbering as BOARD
GPIO.setup(37, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #setting pin 37 of GPIO as input, resistor as pull down
GPIO.setup(35, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #setting pin 35 of GPIO as input, resistor as pull down

def button_callback(channel): #User defined for event detect on pin 37
    print("Button 1 was pushed!") #Printing Button press

def button_callback2(channel): #User defined for event detect on pin 35
    print("Button 2 was pushed!") #Printing Button press

GPIO.add_event_detect(37,GPIO.RISING,callback=button_callback, bouncetime=200) #Detects rising event on pin 37 and calls its function, bouncetime is 200ms
GPIO.add_event_detect(35,GPIO.RISING,callback=button_callback2, bouncetime=200) #Detects rising event on pin 35 and calls its function, bouncetime is 200ms 

try:
        message = input("Press enter to quit\n\n") #Pressing Enter will Quit the program
except:
    pass

finally:
    GPIO.cleanup() #Cleaning Up GPIO pins for Safety