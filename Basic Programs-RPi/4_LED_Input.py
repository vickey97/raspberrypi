#Author: Vivek Patel
#Last Edited: 29/07/2018

import RPi.GPIO as GPIO #importing GPIO library of RaspberryPi
import time #importing time library to add delay

GPIO.setmode(GPIO.BOARD) #Setting GPIO Pin Numbering as BOARD
GPIO.setup(7, GPIO.OUT) #setting pin 7 of GPIO as output
GPIO.setup(11, GPIO.OUT) #setting pin 11 of GPIO as output
GPIO.setup(13, GPIO.OUT) #setting pin 13 of GPIO as output
GPIO.setup(15, GPIO.OUT) #setting pin 15 of GPIO as output

try:
    while 1:
        delay=0.5 #Waiting for 0.5 sec
        i=int(input("Enter Pin Number:")) #Takeing input from user
        print(i) #Printing i
        GPIO.output(i,GPIO.HIGH) #Turning entered LED on
        i=int(input("Enter Pin Number:")) #Taking input from user
        print(i) #Printing i
        GPIO.output(i,GPIO.LOW) #Turning entered LED off

except KeyboardInterrupt:
    pass

finally:
    GPIO.cleanup() #Cleaning Up GPIO pins for Safety