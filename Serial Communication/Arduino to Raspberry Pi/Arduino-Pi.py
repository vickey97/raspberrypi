#Author: Vivek Patel
#Last Edited: 07/01/2019

import serial #importing serial library to use serial
import RPi.GPIO as GPIO #importing GPIO library of RaspberryPi
import time #importing time library to add delay

GPIO.setmode(GPIO.BOARD) #Setting GPIO Pin Numbering as BOARD
GPIO.setup(11, GPIO.OUT) #setting pin 11 of GPIO as output,

ser=serial.Serial("/dev/ttyACM0",9600)  #Setting address, change ACM number as found from ls /dev/tty*
ser.baudrate=9600 #setting baudrate of serial communiaction

def blink(pin): #user defined function of blinking LED
    GPIO.output(pin,GPIO.LOW) #turning LED off
    time.sleep(0.25) #waiting for 0.25 sec
    GPIO.output(pin,GPIO.HIGH) #turning LED on
    time.sleep(0.25) #waiting for 0.25 sec 
    return

try:
    while True:
        read_ser=ser.readline() #reading the recieved value
        print(read_ser) #printing the recieved value
        if(read_ser==b'Hello From Arduino Uno\r\n'):
            blink(11) #calls blink function on pin 11

except:
    pass

finally:
    GPIO.cleanup() #cleaning up GPIO pins at end
