//Author: Vivek Patel
//Last Edited: 05/01/2019

int serIn;
void setup() {
  Serial.begin(9600);
}

void loop() {
  if (Serial.available() > 0) {
    serIn = Serial.read();
    Serial.print(char(serIn));
  }
}