#Author: Vivek Patel
#Last Edited: 7/01/2019
#This program will continuously send char "A" to arduino via serial

import serial #importing serial library for serial communication
import time #importing time library for delay

ser = serial.Serial("/dev/ttyACM0",9600,timeout=1) #Setting address and timeout
ser.flushInput()

while 1: #infinite loop
    ser.write("A".encode())
    time.sleep(1) #wait for 1 sec